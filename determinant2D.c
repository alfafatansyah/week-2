#include <stdio.h>

int a[2][2] = {{10, 20},
               {30, 40}};

void printmatrix();
int determinant(int x [2][2]);

int main()
{
    printmatrix();
    int det = determinant(a);

    printf("\n\ndet |A| = %d\n", det);

    return 0;
}

void printmatrix()
{
    printf("\nMatrix A:\n");

    for (int i = 0; i < 2; i++)
    {
        printf("\n");
        for (int j = 0; j < 2; j++)
            printf("%d ", a[i][j]);
    }
}

int determinant(int x [2][2])
{
    return x[0][0] * x[1][1] - x[0][1] * x[1][0];
}