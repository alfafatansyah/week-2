#include <stdio.h>

typedef struct myStructure
{
    int x;
    int y;
} mystruct_t;

mystruct_t gradient(mystruct_t a, mystruct_t b, mystruct_t c, mystruct_t d, mystruct_t e);

int main()
{
    mystruct_t s0;
    mystruct_t s1;
    mystruct_t s2;
    mystruct_t s3;
    mystruct_t s4;

    mystruct_t result;

    s0.x = 0;
    s0.y = 0;

    s1.x = 5;
    s1.y = 0;

    s2.x = 0;
    s2.y = 7;

    s3.x = -10;
    s3.y = 0;

    s4.x = 0;
    s4.y = -3;

    result = gradient(s0, s1, s2, s3, s4);

    printf("%d, %d", result.x, result.y);

    return 0;
}

mystruct_t gradient(mystruct_t a, mystruct_t b, mystruct_t c, mystruct_t d, mystruct_t e){
    mystruct_t temp;
    temp.x = a.x + b.x;
    temp.y = a.y + b.y;

    temp.x = temp.x + c.x;
    temp.y = temp.y + c.y;

    temp.x = temp.x + d.x;
    temp.y = temp.y + d.y;

    temp.x = temp.x + e.x;
    temp.y = temp.y + e.y;

    return temp;
}