#include <stdio.h>

int numb[] = {5, 2, 8, 7, 1};
int temp = 0;

int main()
{
    int tnumb = sizeof(numb) / sizeof(numb[0]);

    for (int i = 0; i < tnumb; i++)
    {
        printf("%d ", numb[i]);
    }
    printf("\n");

    for (int i = 0; i < tnumb; i++)
    {
        for (int j = i + 1; j < tnumb; j++)
        {
            if (numb[i] > numb[j])
            {
                temp = numb[i];
                numb[i] = numb[j];
                numb[j] = temp;

                for (int i = 0; i < tnumb; i++)
                {
                    printf("%d ", numb[i]);
                }

                printf("\n");
            }
        }
    }

    return 0;
}