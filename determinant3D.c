#include <stdio.h>

int a[3][3] = {{10, 20, 30},
               {35, 55, 77},
               {99, 87, 35}};

void printmatrix();
int  determinant(int x [3][3]);

int main()
{
    printmatrix();
    int det = determinant(a);

    printf("\n\ndet |A| = %d\n", det);

    return 0;
}

void printmatrix()
{
    printf("\nMatrix A:\n");

    for (int i = 0; i < 3; i++)
    {
        printf("\n");
        for (int j = 0; j < 3; j++)
            printf("%d ", a[i][j]);
    }
}

int determinant(int x [3][3])
{
    int i = (x[1][1] * x[2][2]) - (x[1][2] * x[2][1]);
    int j = (x[1][0] * x[2][2]) - (x[1][2] * x[2][0]);
    int k = (x[1][0] * x[2][1]) - (x[1][1] * x[2][0]);

    int det = (x[0][0] * i) - (x[0][1] * j) + (x[0][2] * k);
    return det;

    /* Metode sarrus
    int r = a[0][0] * a[1][1] * a[2][2];
    int s = a[0][1] * a[1][2] * a[2][0];
    int t = a[0][2] * a[1][0] * a[2][1];
    int u = a[0][2] * a[1][1] * a[2][0];
    int v = a[0][0] * a[1][2] * a[2][1];
    int w = a[0][1] * a[1][0] * a[2][2];

    int det = r + s + t - u - v - w;
    */
}