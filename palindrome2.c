#include <stdio.h>
#include <string.h>

char kata[] = "tenet";
// char kata[] = "";
char katabalik[] = "";
int a;

int main()
{
    // printf("\n Masukkan 1 kata : ");
    // scanf("%s", kata);
    printf("\n kata %s ", kata);

    strcpy(katabalik, kata);
    strrev(katabalik);

    a = strcmp(kata, katabalik);

    if (a == 0)
        printf("merupakan palindrome \n");
    else
        printf("bukan merupakan palindrome \n");

    return 0;
}