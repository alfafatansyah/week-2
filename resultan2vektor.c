#include <stdio.h>
#include <math.h>
#define PI 3.1415925

typedef struct myvector
{
    double F1, F2, cos, R;

} myvec_t;

double res2vec(myvec_t a);
double sel2vec(myvec_t a);

int main()
{
    myvec_t R1;

    R1.F1 = 8.0;
    R1.F2 = 4.0;
    R1.cos = 60;
    
    R1.R = res2vec(R1);
    printf("\nResult vector %.2f + %.2f = %.2f\n", R1.F1, R1.F2, R1.R);

    R1.R = sel2vec(R1);
    printf("\nResult vector %.2f + %.2f = %.2f\n", R1.F1, R1.F2, R1.R);

    return 0;
}

double res2vec(myvec_t a)
{

    double temp;
    printf("\na.cos = %f", a.cos);
    temp = pow(a.F1, 2) + pow(a.F2, 2);
    a.cos = cos(a.cos * (PI / 180.0));
    temp = temp + (2 * a.F1 * a.F2) * a.cos;
    temp = sqrt (temp);
    return temp;
}

double sel2vec(myvec_t a)
{
    double temp;
    printf("\na.cos = %f", a.cos);

    temp = pow(a.F1, 2) + pow(a.F2, 2);
    a.cos = cos(a.cos * (PI / 180.0));
    temp = temp - (2 * a.F1 * a.F2) * a.cos;
    temp = sqrt (temp);
    return temp;
}