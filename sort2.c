#include <stdio.h>

int numb[] = {5, 2, 8, 7, 1};
int temp, a;

int main()
{
    int tnumb = sizeof(numb) / sizeof(numb[0]);
    
    for (int i = 0; i < tnumb; i++)
        printf("%d", numb[i]);

    printf("\n");

    for (int i = 0; i < tnumb - 1; i++)
    {
        if (numb[i] > numb[i + 1])
        {
            temp = numb[i];
            numb[i] = numb[i + 1];
            numb[i + 1] = temp;

            for (int i = 0; i < tnumb; i++)
                printf("%d", numb[i]);

            printf("\n");

            i = -1;
        }
    }

    return 0;
}