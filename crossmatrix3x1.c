#include <stdio.h>

int a[] = {1, 2, 3};
int b[] = {1, -1, 2};

int ab[3];

int crossresult;

int crossmatrix(int x[], int y[]);
void printcrossmatrix();

int main()
{
    //crossmatrix();
    printcrossmatrix();

    return 0;
}

/*int crossmatrix(int x[], int y[])
{
    ab[0] = (x[1] * y[2]) - (x[2] * y[1]);
    ab[1] = (x[2] * y[0]) - (x[0] * y[2]);
    ab[2] = (x[1] * y[0]) - (x[0] * y[1]);
}*/

void printcrossmatrix()
{
    printf("\nMatrix A:");

    for (int i = 0; i < 3; i++)
    {
        printf("\n");
        printf("%d ", a[i]);
    }

    printf("\nMatrix B:");

    for (int i = 0; i < 3; i++)
    {
        printf("\n");
        printf("%d ", b[i]);
    }

    printf("\nMatrix AxB=");

    for (int i = 0; i < 3; i++)
    {
        printf("\n");
        printf("%d ", ab[i]);
    }
}