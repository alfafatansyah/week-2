#include <stdio.h>
#define num_0 0b00111111 // 0x3f
#define num_1 0x00000110 // 0x06
#define num_2 0b01011011 // 0x5B
#define num_3 0b01001111 // 0x4F
#define num_4 0b01100110 // 0x66
#define num_5 0b01101101 // 0x6D
#define num_6 0b01111101 // 0x7D
#define num_7 0b00000111 // 0x07
#define num_8 0b01111111 // 0x7F
#define num_9 0b01101111 // 0x6F

int a = num_5;

int sender(int x);
int receiver(int x);

int main()
{
    int temp = sender(a);

    temp = receiver(temp);

    printf("\n%d\n", temp);

    return 0;
}

int sender(int x)
{
    int b = 0;
    for (int i = 1 << 7; i > 0; i = i >> 1)
    {
        b <<= 1;
        b = (x & i) ? b | 1 : b | 0;
        //printf("%d\n", b);
    }

    return b;
}

int receiver(int x)
{
    switch (x)
    {
    case num_0:
        return 0;
        break;
    case num_1:
        return 1;
        break;
    case num_2:
        return 2;
        break;
    case num_3:
        return 3;
        break;
    case num_4:
        return 4;
        break;
    case num_5:
        return 5;
        break;
    case num_6:
        return 6;
        break;
    case num_7:
        return 7;
        break;
    case num_8:
        return 8;
        break;
    case num_9:
        return 9;
        break;
    }
}