#include <stdio.h>

int a[] = {2, 3, 1};
int b[] = {1, 4, 3};

int dotresult;

void dotmatrix();
void printdotmatrix();

int main()
{
    dotmatrix();
    printdotmatrix();

    return 0;
}

void dotmatrix()
{
    int temp;
    for (int i = 0; i < 3; i++)
    {
        temp = temp + (a[i] * b[i]);
    }

    dotresult = temp;
}

void printdotmatrix()
{
    printf("\nMatrix A:");

    for (int i = 0; i < 3; i++)
    {
        printf("\n");
        printf("%d ", a[i]);
    }

    printf("\nMatrix B:");

    for (int i = 0; i < 3; i++)
    {
        printf("\n");
        printf("%d ", b[i]);
    }

    printf("\nMatrix A●B= %d\n", dotresult);
    
}