#include <stdio.h>

int input = 2;

int persegi(int x);

int main()
{
    printf("%d", persegi(input));
}

int persegi(int x)
{
    return x ? persegi(x - 1) + x + x - 1 : 0;
}