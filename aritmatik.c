#include <stdio.h>

int a = 9;
int b = 7;

int add(int x, int y);
int sub(int x, int y);

int main()
{
    int eqadd = add(a, b);
    int eqsub = sub(a, b);

    printf("\n%d + %d = %d \n", a, b, eqadd);
    printf("%d - %d = %d \n", a, b, eqsub);

    return 0;
}

int add(int x, int y)
{
    int xor, carry;
    xor = x ^ y;
    carry = x & y;
    if (carry == 0)
        return xor;
    else
        return add(xor, carry << 1);
}

int sub(int x, int y)
{
    int xor, borrow;
    xor = x ^ y;
    borrow = (~x) & y;
    if (borrow == 0)
        return xor;
    else
        return sub(xor, borrow << 1);
}