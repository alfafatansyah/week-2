#include <stdio.h>

const float n = 100;

float akar(float x);

void main()
{
    printf("akar dari %f adalah %f", n, akar(n));
}

float akar(float x)
{
    int xn;
    do
    {
        xn = x;
        x = (x + n / x) / 2;
        printf("%f\n", x);
    } while (xn != x);

    return x;
}