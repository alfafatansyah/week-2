#include <stdio.h>

int x = 8;
int y;

int sevensegment(int a);
void printresult();

int main()
{
    y = sevensegment(x);
    printresult();

    return 0;
}

int sevensegment(int a)
{
    switch (a)
    {
    case 0:
        return 0x00;
        break;
    case 1:
        return 0x06;
        break;
    case 2:
        return 0x5B;
        break;
    case 3:
        return 0x4F;
        break;
    case 4:
        return 0x66;
        break;
    case 5:
        return 0x6D;
        break;
    case 6:
        return 0x7D;
        break;
    case 7:
        return 0x07;
        break;
    case 8:
        return 0x7F;
        break;
    case 9:
        return 0x6F;
        break;
    }
}

void printresult()
{
    printf("\nNilai Desimal = %d\n", x);

    for (int i = 1 << 7; i > 0; i = i / 2)
        (y & i) ? printf("1") : printf("0");

    printf(" (0x%X)\n", y);
}