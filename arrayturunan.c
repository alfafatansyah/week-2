#include <stdio.h>
#include <string.h>

typedef struct myschool
{
    char nama[6];
    int umur;
} mystuden_t;

void rankbyage(mystuden_t x[]);

int main()
{

    mystuden_t siswa[10];

    strcpy(siswa[0].nama, "punk01");
    siswa[0].umur = 15;
    strcpy(siswa[1].nama, "punk02");
    siswa[1].umur = 16;
    strcpy(siswa[2].nama, "punk03");
    siswa[2].umur = 17;
    strcpy(siswa[3].nama, "punk04");
    siswa[3].umur = 18;
    strcpy(siswa[4].nama, "punk05");
    siswa[4].umur = 17;
    strcpy(siswa[5].nama, "punk06");
    siswa[5].umur = 16;
    strcpy(siswa[6].nama, "punk07");
    siswa[6].umur = 15;
    strcpy(siswa[7].nama, "punk08");
    siswa[7].umur = 16;
    strcpy(siswa[8].nama, "punk09");
    siswa[8].umur = 17;
    strcpy(siswa[9].nama, "punk10");
    siswa[9].umur = 18;

    rankbyage(siswa);

    return 0;
}

void rankbyage(mystuden_t x[])
{
    mystuden_t temp;
    for (int i = 0; i < 10 - 1; i++)
    {
        if (x[i].umur < x[i + 1].umur)
        {
            temp = x[i];
            x[i] = x[i + 1];
            x[i + 1] = temp;

            i = -1;
        }
    }

    for (int i = 0; i < 10; i++)
    {
        printf("%s umur %d", x[i].nama,x[i].umur);

        printf("\n");
    }
}
