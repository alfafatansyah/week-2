#include <stdio.h>

int a[2][2] = {{2, -2},
               {1, 3}};
int b[2][2] = {{2, -2},
               {1, 3}};

int ab[2][2];

int dotresult;

void dotmatrix();
void printdotmatrix();

int main()
{
    dotmatrix();
    printdotmatrix();

    return 0;
}

void dotmatrix()
{
    int temp;
    for (int i = 0; i < 2; i++)
    {
        for (int j = 0; j < 2; j++)
        {
            ab[i][j] = (a[i][0] * b[0][j]) + (a[i][1] * b[1][j]);
        }
    }
    printf("\n");

    dotresult = temp;
}

void printdotmatrix()
{
    printf("\nMatrix A:");

    for (int i = 0; i < 2; i++)
    {
        printf("\n");
        for (int j = 0; j < 2; j++)
            printf("%d ", a[i][j]);
    }

    printf("\nMatrix B:");

    for (int i = 0; i < 2; i++)
    {
        printf("\n");
        for (int j = 0; j < 2; j++)
            printf("%d ", b[i][j]);
    }

    printf("\nMatrix A●B=");

    for (int i = 0; i < 2; i++)
    {
        printf("\n");
        for (int j = 0; j < 2; j++)
            printf("%d ", ab[i][j]);
    }

    /*printf("\nPerhitungan Matrix A●B:");

    for (int i = 0; i < 2; i++)
    {
        for (int j = 0; j < 2; j++)
            printf("\n(%d x %d) + (%d x %d) = %d", a[i][0], b[0][i], a[i][1], b[1][j], ab[i][j]);
    }*/

    printf("\n");
}